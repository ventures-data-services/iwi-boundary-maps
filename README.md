# Iwi Boundary Maps

Provides geojson map layers for iwi boundaries. Based on shapefile data supplied by TPK.
Shapefiles are annoying to work with, and not as flexible or easy to use with libraries like [leaflet.js](https://leafletjs.com/).

There are definitely some geojson files floating around, but I couldn't find any single place to get them for all iwi so - here they are.

## Quick start

The actual geojson files for all iwi are available in this repo, under the `geojson_simplified` folder. Note that these have been simplified to save space. This works fine for many iwi layers, but for some (Ngāti Tūwharetoa) it is too aggressive and important parts, such as Tokorangi go missing.

## Making non-simplified high detail layers

The provided R script can be used to generate high detail geojson layers for specific, or all, iwi. You will need a working version of R (tested with 4.2.0-4) and the libraries:
* rgdal
* spdplyr
* geojsonio
* rmapshaper
* proj4
* rgeos
* stringi

The provided R script `iwi_geojson.R` can be edited to suit your needs. Run the script up to line 43 to generate a high detail geojson file for every iwi. Alternatively you can specify an iwi on line 32 and just run the code between lines 30-41 to generate a geojson file for that iwi only. To find out the TPK code for the iwi you want, just browse the df dataframe (`View(df)`).

## Visualisation

The `iwi_geojson.R` script will also allow you to visualise a given geojson layer with map tiles using the `leaflet` library. Just specify the filename you want to view on line 48, and run the rest of the code.

## Support

Please make an issue if you have any questions.

## Acknowledgment
Big thanks to TPK for making the base shapefile publicly available. No licence was utilised in that data, so none is implemented here.
